import React              from 'react';
import ReactDOM           from 'react-dom';
import './index.css';
import App                from './App';
import * as serviceWorker from './serviceWorker';

// ReactDom.render, se le pasa 2 parametros; que va inprimir(App) y donde lo va imprimir(root)
ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();