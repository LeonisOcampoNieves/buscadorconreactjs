import React, { Component } from 'react';
import Buscador             from './components/Buscador'
import Resultado            from './components/Resultado'

class App extends Component {
	state = {
		termino  : '',
		imagenes : []
	}

	scroll = () => {
		const elemento = document.querySelector('.jumbotron')

		// Nos lleva hasta arriba
		elemento.scrollIntoView('smooth', 'start')
	}

	paginaAnterior = () => {
		// Leer el state de la página actual
		let pagina = this.state.pagina

		// Si la página es 1, ya no ir hacia atras
		if(pagina === 1) return null // Se detiene la ejecución

		// Resto uno a la página actual
		pagina -= 1

		// agregar el cambio al state
		this.setState({
			pagina
		}, () => {
			this.consultarApi()
			this.scroll()
		})
	}

	paginaSiguiente = () => {
		// Leer el state de la página actual
		let pagina = this.state.pagina

		// Sumar uno a la página actual
		pagina += 1

		// agregar el cambio al state
		this.setState({
			pagina
		}, () => {
			this.consultarApi()
			this.scroll()
		})
	}

	consultarApi = () => {
		const termino = this.state.termino
		const pagina  = this.state.pagina

		const url = `https://pixabay.com/api/?key=94555-5b42dfb0b9ef2f4941fd4a006&q=${termino}&per_page=30&page=${pagina}`

		fetch(url)
			.then(respuesta => respuesta.json())
			.then(resultado => this.setState({ imagenes: resultado.hits }))
	}
	
	datosBusqueda = (termino) => {
		this.setState({
			termino : termino,
			pagina: 1

		}, () => {
			this.consultarApi()
		})
	}

	render() {
		return (
			<div className="app container">
				<div className="jumbotron">
					<p className="lead text-center">Buscador de imágenes</p>
	
					<Buscador
						datosBusqueda={this.datosBusqueda}
					/>
				</div>

				<div className="row justify-content-center">
					<Resultado
						imagenes        = {this.state.imagenes}
						paginaAnterior  = {this.paginaAnterior}
						paginaSiguiente = {this.paginaSiguiente}
					/>
				</div>
			</div>
		  );
	}
}

export default App;