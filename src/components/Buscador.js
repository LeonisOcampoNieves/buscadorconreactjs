import React, { Component } from 'react'

class Buscador extends Component {
    // Podemos acceder a lo que el usuario escriba en el formulario con esta propiedad
    busquedaRef = React.createRef()

    obtenerDatos = (e) => {
        // previene la acción por default del formulario(texto en la parte superios -barra de dirección)
        e.preventDefault()

        // Tomamos el valor del input y lo enviamos al componente principal
        const termino = this.busquedaRef.current.value

        // Psar datos del hijo(Buscador) al padre(App)
        this.props.datosBusqueda(termino)
    }

    render() {
        return(
            // con onSubmit llamo al metodo obtenerDatos
            <form onSubmit={this.obtenerDatos}>
                <div className="row">
                    <div className="form-group col-md-8">
                        <input ref={this.busquedaRef} type="text" className="form-control 
                        form-control-lg" placeholder="Busca tu Imágen. Ejemplo: Futbol"/>
                    </div>
                </div>

                <div className="row">
                    <div className="form-group col-md-4">
                        <input type="submit" className="btn btn-lg btn-danger btn-block" value="Buscar"/>
                    </div>
                </div>
            </form>
        )
    }
}

export default Buscador